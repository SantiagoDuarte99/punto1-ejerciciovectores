/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Estudiante;
import Modelo.Materia;
import ufps.util.varios.ArchivoLeerURL;

/**
 * Tenemos 3 materias
 *
 * @author madarme
 */
public class SistemaInformacion {

    private Materia myMaterias[] = new Materia[3];
    private String urlProgramacionIII;
    private String urlMatematicaIII;
    private String urlFisicaII;

    public SistemaInformacion() {
    }

    /**
     * Solución del punto 1
     *
     * @param urlProgramacionIII cadena con la url con la información de
     * estudiantes de programación III
     * @param urlMatematicaIII cadena con la url con la información de
     * estudiantes de matemáticas III
     * @param urlFisicaII cadena con la url con la información de estudiantes de
     * física II
     */
    public SistemaInformacion(String urlProgramacionIII, String urlMatematicaIII, String urlFisicaII) {
        this.urlProgramacionIII = urlProgramacionIII;
        this.urlMatematicaIII = urlMatematicaIII;
        this.urlFisicaII = urlFisicaII;

        this.myMaterias[0] = crearMateria(1, "Matemáticas III", this.urlMatematicaIII);
        this.myMaterias[1] = crearMateria(2, "Fisica II", this.urlFisicaII);
        this.myMaterias[2] = crearMateria(3, "Progam III", this.urlProgramacionIII);
    }

    /**
     * Crea un materia
     *
     * @param codigo un entero con el código de la materia
     * @param nombreMateria una cadena que contiene el nombre de la materia
     * @param url una cadena que contiene la URL de un archivo csv con la
     * información de los estudiantes de esa materia
     */
    private Materia crearMateria(int codigo, String nombreMateria, String url) {
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object datos[] = archivo.leerArchivo();
        Estudiante estudiantes[] = new Estudiante[datos.length - 1];

        int j = 0;

        for (int i = 1; i < datos.length; i++) {

            estudiantes[j] = Separador(datos[i].toString());
            j++;

        }

        Materia m = new Materia(codigo, nombreMateria, estudiantes);

        return m;

    }

    /**
     * Punto 2.	Crear un vector con los estudiantes que tienen matriculado
     * programación III, física II pero no Matemáticas III.
     *
     * @return un listado de estudiantes
     */
    public Estudiante[] getListadoProgIII_FisicaII_NO_MatIII() {
        Estudiante fisicaprogramacion[] = new Estudiante[myMaterias[1].getMyEstudiantes().length];

        for (int i = 0; i < myMaterias[1].getMyEstudiantes().length; i++) {

            for (int j = 0; j < myMaterias[2].getMyEstudiantes().length; j++) {

                if (myMaterias[1].getMyEstudiantes()[i].equals(myMaterias[2].getMyEstudiantes()[j]) == false) {

                    fisicaprogramacion[i] = myMaterias[1].getMyEstudiantes()[i];

                }

            }

        }

        return fisicaprogramacion;
    }

    /**
     * Punto 3.	Crear un vector con los estudiantes que tienen matriculado
     * programación III y Matemáticas III pero no física II.
     *
     * @return
     */
    public Estudiante[] getListadoProgIII_MatIII_NO_fisII() {

        Estudiante mateprogramacion[] = new Estudiante[myMaterias[0].getMyEstudiantes().length];

        Estudiante mateprogramacionsinfisica[] = new Estudiante[myMaterias[0].getMyEstudiantes().length];

        for (int i = 0; i < myMaterias[0].getMyEstudiantes().length; i++) {

            for (int j = 0; j < myMaterias[2].getMyEstudiantes().length; j++) {

                if (myMaterias[0].getMyEstudiantes()[i].equals(myMaterias[2].getMyEstudiantes()[j])) {

                    mateprogramacion[i] = myMaterias[2].getMyEstudiantes()[i];

                }

            }

        }

        for (int i = 0; i < mateprogramacion.length; i++) {

            for (int j = 0; j < myMaterias[1].getMyEstudiantes().length; j++) {

                if (!mateprogramacion[i].equals(myMaterias[1].getMyEstudiantes()[j])) {

                    mateprogramacionsinfisica[i] = mateprogramacion[i];

                }

            }

        }

        return mateprogramacion;
    }

    /**
     * Punto 4.	Crear un vector con los estudiantes que están viendo dos de las
     * tres materias (Cualquiera que sea).
     *
     * @return
     */
    public Estudiante[] getListadoVer_UNICAMENTE_dosMaterias() {

        Estudiante v[] = new Estudiante[myMaterias[1].getMyEstudiantes().length];

        for (int i = 0; i < myMaterias[1].getMyEstudiantes().length; i++) {

            for (int j = 0; j < myMaterias[0].getMyEstudiantes().length; j++) {

                if (myMaterias[1].getMyEstudiantes()[i].equals(myMaterias[0].getMyEstudiantes()[j])) {

                    v[i] = myMaterias[1].getMyEstudiantes()[i];

                }

            }
        }

        return v;
    }

    /**
     * Obtiene la información de todos los estudiantes por materia
     *
     * @return una cadena con los 3 listados de las materias
     */
    @Override
    public String toString() {

        return "SistemaInformacion{" + "myMaterias=" + myMaterias + ", urlProgramacionIII=" + urlProgramacionIII + ", urlMatematicaIII=" + urlMatematicaIII + ", urlFisicaII=" + urlFisicaII + '}';

    }

    public Estudiante Separador(String e) {

        String b[] = e.split(";");
        Estudiante estudiante = null;

        estudiante = new Estudiante(Integer.parseInt(b[0]), b[1], b[2]);

        return estudiante;
    }

    public boolean verificacion(Materia[] myMaterias) throws Exception {

        boolean a = false;

        if (this.myMaterias == null) {

            throw new Exception(" El vector no tiene ningun elemento ");

        } else {

            for (int i = 0; i < myMaterias.length; i++) {

                for (int j = 0; j < myMaterias.length; j++) {

                    if (myMaterias[i].equals(myMaterias[j])) {

                        a = true;

                        throw new Exception("Si hay estudiantes repetidos");

                    }

                }

            }

        }
        return a;

    }

}
