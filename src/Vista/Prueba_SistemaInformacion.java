/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.SistemaInformacion;
import java.util.Arrays;

/**
 *
 * @author madar
 */
public class Prueba_SistemaInformacion {

    public static void main(String[] args) {
        String url1 = "https://gitlab.com/estructuras-de-datos-i-sem-2019/persistencia/listadoestudiantes/-/raw/master/listadoProgramacionIII.csv";
        String url2 = "https://gitlab.com/estructuras-de-datos-i-sem-2019/persistencia/listadoestudiantes/-/raw/master/matematicaIII.csv";
        String url3 = "https://gitlab.com/estructuras-de-datos-i-sem-2019/persistencia/listadoestudiantes/-/raw/master/fisicaII.csv";

        SistemaInformacion mySistema = new SistemaInformacion(url1, url2, url3);

        System.out.println("estudiantes que estan en programacion y fisica");

        System.out.println(Arrays.toString(mySistema.getListadoProgIII_FisicaII_NO_MatIII()));
        System.out.println("");
        System.out.println("estudiantes que estan en programacion y matematicas");

        System.out.println(Arrays.toString(mySistema.getListadoProgIII_MatIII_NO_fisII()));
        System.out.println("");
        System.out.println(Arrays.toString(mySistema.getListadoVer_UNICAMENTE_dosMaterias()));
        /**
         * :) Debe llamar los métodos e imprimir el resultado de los vectores
         * resultantes
         *
         */
    }
}
