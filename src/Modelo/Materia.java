/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madar
 */
public class Materia {
    
    private int codMateria;
    private String nombreMateria;
    private Estudiante myEstudiantes[];
    
    public Materia(){};
    
    public Materia(int codMateria,String nombreMateria,Estudiante myEstudiantes[]){
    
        this.codMateria=codMateria;
        this.nombreMateria=nombreMateria;
        this.myEstudiantes= myEstudiantes;
    }

    public int getCodMateria() {
        return codMateria;
    }

    public void setCodMateria(int codMateria) {
        this.codMateria = codMateria;
    }

    public Estudiante[] getMyEstudiantes() {
        return myEstudiantes;
    }

    public void setMyEstudiantes(Estudiante[] myEstudiantes) {
        this.myEstudiantes = myEstudiantes;
    }
    
    public boolean verificacion(Estudiante[] myEstudiantes) throws Exception{
        
        boolean a=false;
        
        if(this.myEstudiantes==null){
        
        throw new Exception(" El vector no tiene ningun elemento ");
        
        }else{
        
        for(int i=1;i<myEstudiantes.length;i++){
            
            for(int j=1;j<myEstudiantes.length;j++){
        
        if(myEstudiantes[i].equals(myEstudiantes[j])){
            
            a=true;
            
            throw new Exception("Si hay estudiantes repetidos");
            
        }
            
        }
            
        }
        
        }
        return a;
 
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Materia other = (Materia) obj;
        if (this.codMateria != other.codMateria) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Materia{" + "codMateria=" + codMateria + ", nombreMateria=" + nombreMateria + ", myEstudiantes=" + myEstudiantes + '}';
    }
    
    
}
